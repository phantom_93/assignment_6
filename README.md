Assignment 6 - YellowPages 2.0
--------------------------------------------------
Upgrade your YellowPages solution
Have a class that describes a person 
Reminder: It will need it’s own source file 
First Name, Last Name, Telephone, etc 
Use overloaded constructors
Create a collection of this new class 
Modify your search to use a behaviour within this class. The behavior implementation is up to you.
Weight: Necessary 