﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_6
{
    class Contact
    {
        // properties 
        #region Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }
        #endregion

        #region Constructors 
        // constructors with method overloading 
        public Contact(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
            Phone = "Not provided";
            Address = "Not provided";
        }


        public Contact(string firstName, string lastName, string phone)
        {
            FirstName = firstName;
            LastName = lastName;
            Phone = phone;
            Address = "Not provided";
        }

        public Contact(string firstName, string lastName, string phone, string address)
        {
            FirstName = firstName;
            LastName = lastName;
            Phone = phone;
            Address = address;
        }
        #endregion

        #region string methods
        public override string ToString()
        {
            return $"Contact(First Name: {FirstName}, Last Name: {LastName}, Phone Number: {Phone}, Address: {Address})";
        }
        #endregion 
    }
}