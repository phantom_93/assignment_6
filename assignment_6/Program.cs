﻿using System;
using System.Collections.Generic;

namespace assignment_6
{
    class Program
    {
        static List<Contact> contacts;

        static void Main(string[] args)
        {
            #region General
            // registry of stringList 
            string[] people = {
                "Anna,Banana,47895600,JohnStreet 3" ,
                "Breaking,Bad,89457800, OsloCity 5" ,
                "Anna,Potato,98765432, Bergen 6",
                "John,Doe,45692103 ,Stavanger 3" ,
                "Jane,Doe,46783421,Grimstad 6"
            };

            contacts = new List<Contact>();
            CreateContacts(people);

            Console.WriteLine("Registered contacts are:");
            foreach (Contact contact in contacts)
            {
                //Console.WriteLine(item);
                Console.WriteLine(contact.ToString());
            }
            UserInput();
            #endregion 
        }
        #region CreateContacts method
        // this function uses to create contacts 
        static void CreateContacts(params string[] contactString)
        {
            foreach (string contactStr in contactString)
            {
                string[] details = contactStr.Split(',');

                Contact contact = new Contact(details[0], details[1], details[2], details[3]);
                contacts.Add(contact);
            }
        }
        #endregion


        #region UserInput method 
        // the following function handles UserInput method
        static void UserInput()
        {

            bool choice = true;
            while (choice)
            {
                Console.WriteLine("enter 'p' to continue and 'e' to exit");
                Console.WriteLine("Enter a character/characters for partial or full matches. (p)");
                Console.WriteLine("Exit (e)");
                string menu = Console.ReadLine();
                switch (menu)
                {
                    case "p":
                        FindMatches();
                        break;
                    case "e":
                    default:
                        choice = false;
                        break;
                }
            }


        }
        #endregion


        #region FindMatches
        // this function checks if the given name by user is in the registry or not 
        static void FindMatches()
        {
            System.Console.WriteLine("Enter a character");
            string input = Console.ReadLine().ToLower();

            bool found = false;
            foreach (Contact contact in contacts)
            {
                string fullName = $"{contact.FirstName} {contact.LastName}";

                if (contact.FirstName.ToLower().Contains(input)|| contact.LastName.ToLower().Contains(input)|| fullName.ToLower().Contains(input))
                {
                    found = true;
                    Console.WriteLine($"Found contact: {fullName}");
                }
                // Phone Number
                else if (contact.Phone.Contains(input))
                {
                    found = true;
                    Console.WriteLine($"Found contact: {fullName}");
                }
                // Address
                else if (contact.Address.ToLower().Contains(input))
                {
                    found = true;
                    Console.WriteLine($"Found contact: {fullName}");
                }
            }

            if (!found)
                System.Console.WriteLine("Contact not found");
        }
        #endregion
    }
}